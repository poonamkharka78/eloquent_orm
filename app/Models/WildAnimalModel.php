<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WildAnimalModel extends Model
{
    public $table = "wild_animals";

	  public function animal(){
	  	
	  	return $this->belongsTo('App\Models\Animal','cat_id','id');
	  
	  }

	  public function BreedModel(){
	  	
	  	return $this->belongsTo('App\Models\BreedModel','breed_id','id');
	  
	  }
}
