<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DomesticAnimalModel extends Model
{
    public $table = "domestic_animals";

	  public function Animal(){
	  	
	  	return $this->belongsTo('App\Models\Animal','cat_id','id');
	  
	  }

	  public function BreedModel(){
	  	
	  	return $this->belongsTo('App\Models\BreedModel','breed_id','id');
	  
	  }
}
