<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterRole extends Model
{
    public $table = "role_masters";

    public  function rolesdata(){
        return $this->hasMany('App\Models\RoleModel','id','role_id');
    }

    public function  persondata(){
        return $this->hasMany('App\Models\PersonModel','id','person_id');
    }


}
