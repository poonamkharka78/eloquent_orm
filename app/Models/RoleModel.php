<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
    public $table = "roles";

    public function persons(){
        return $this->hasMany('App\Models\PersonModel','role_id','id');
    }

    public function rolemaster(){
        return $this->belongsToMany('App\Models\MasterRole','role_masters','id','role_id');
    }
}
