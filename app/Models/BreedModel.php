<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BreedModel extends Model
{
    public $table = "breed";

    public function DomesticAnimalModel(){
	  	
	  	return $this->hasOne('App\Models\DomesticAnimalModel','breed_id','id');
	  
	  }

	  public function WildAnimalModel(){
	  	
	  	return $this->hasOne('App\Models\WildAnimalModel','breed_id','id');
	  
	  }
}
