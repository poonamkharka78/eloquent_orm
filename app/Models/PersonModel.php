<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonModel extends Model
{
    public $table = "persons";

    public function roles(){
        return $this->belongsTo('App\Models\RoleModel','role_id','id');
    }

    public function masterrole(){
        return $this->belongsToMany('App\Models\MasterRole','role_masters','id','person_id');
    }



}
