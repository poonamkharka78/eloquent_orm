<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
	protected $table = "animals";

	  public function DomesticAnimalModel(){
	  	
	  	return $this->hasMany('App\Models\DomesticAnimalModel','cat_id','id');
	  
	  }

	  public function WildAnimalModel(){
	  	
	  	return $this->hasMany('App\Models\WildAnimalModel','cat_id','id');
	  
	  }  
}
