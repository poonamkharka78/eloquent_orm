<?php

namespace  app\Helpers;
use Carbon;
class Helper
{
    public static function test($date)
    {
        return \Carbon\Carbon::parse($date)->locale('en_EN')->isoFormat('LLLL');
    }
}
