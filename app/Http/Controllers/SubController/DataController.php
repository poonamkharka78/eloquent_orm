<?php

namespace App\Http\Controllers\SubController;

use App\Models\PersonModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterRole;
use App\Models\RoleModel;


class DataController extends Controller
{
    public function index(){
        $viewData = MasterRole::with('persondata','rolesdata')->get();
        return view('welcome',compact('viewData'));
    }
    public function list(){
        //$data = PersonModel::->with('roles')->get();
        $data = PersonModel::with('roles')->get();
        //dd($data);
        return view('userList',compact('data'));
    }

}
