<?php

namespace App\Http\Controllers\SubController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RoleModel;
use App\Models\PersonModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Session;

class PersonController extends Controller
{
    public function index(){


    	$data = RoleModel::get();
    	return view('register',compact('user','data'));
    }

    public function store(Request $request){

    	$validator = Validator::make($request->all(), [
            'name' => 'required',
            'role_id' => 'required',
            'email'=> 'required|unique:persons',
            'password'=> 'required|min:4',
        ]);

        if ($validator->fails()) {
            return redirect('/signup-page')
                        ->withErrors($validator)
                        ->withInput();
        }
        $raw = new PersonModel;
        $raw->name = $request->name;
        $raw->role_id = $request->role_id;
        $raw->email= $request->email;
        $raw->password = Hash::make($request->password);
        $save = $raw->save();
        if($save){

        	Session::flash('success','Data Submitted');
        	return redirect('signup-page');

        }
        else{
        	Session::flash('fail','Data Not Submitted');
        	return redirect('signup-page');

        }


    }
}
