<?php

namespace App\Http\Controllers\SubController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RoleModel;
use Session;
use Illuminate\Support\Facades\Validator;
class RoleMasterController extends Controller
{
   public function index(){
   		return view("role");
   	}

   	public function store(Request $request){

       $validator = Validator::make($request->all(), [
            'role' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect('/index')
                ->withErrors($validator)
                ->withInput();
        }


   		$data = new RoleModel;
   		$data->role = $request->role;
   		$save = $data->save();

   		if($save){
   			Session::flash('msg', 'Role Added');
   			return redirect('/index');
		}
			else{
				Session::flash('failmsg', 'Role Not Added');
				return redirect('/index');
			}

   	}
}
