<?php

namespace App\Http\Controllers\SubController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Animal;
use App\Models\BreedModel;
use App\Models\WildAnimalModel;
use App\Models\DomesticAnimalModel;

class AminalDataController extends Controller
{
	public function index(){
		$homeData = Animal::where('category','domestic')->get();
		$home = Animal::where('category','wild')->get();
		return view('Eloquent.home',compact('homeData','home'));
	}


    public function domesticData(){
    	$data = Animal::with('DomesticAnimalModel')->where('category','domestic')->get();
        $breed = BreedModel::with('DomesticAnimalModel')->get();
        //dd($data);
    	 return view('Eloquent.domestic',compact('data','breed'));
    }

    public function wildData(){
    	$rawData = Animal::with('WildAnimalModel')->where('category','wild')->get();
        $breedWild = BreedModel::with('DomesticAnimalModel')->get();
    	 return view('Eloquent.wild',compact('rawData','breedWild'));
    }

    public function domeBreed(){
        $breedDog = BreedModel::with('DomesticAnimalModel')->where('Animal_name','Dog')->get();
        $breedDog = BreedModel::with('DomesticAnimalModel')->where('Animal_name','Cat')->get();
        $breedDog = BreedModel::with('DomesticAnimalModel')->where('Animal_name','Cow')->get();
        
        
    }



    
}
