<?php

use Illuminate\Database\Seeder;

class DomesticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('domestic_animals')->insert([
       		'cat_id' => 1,
          'animal_name' => 'Dog',
          'breed_id'=>2,
           
        ]);
       DB::table('domestic_animals')->insert([
       		'cat_id' => 1,
            'animal_name' => 'Cat',
            'breed_id'=>1,
           
        ]);
       DB::table('domestic_animals')->insert([
       		'cat_id' => 1,
            'animal_name' => 'Cow',
            'breed_id'=>3,
           
        ]);
    }
}
