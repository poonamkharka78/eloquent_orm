<?php

use Illuminate\Database\Seeder;

class AnimalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('animals')->insert([
            'category' => 'Domestic',
            
        ]);

       DB::table('animals')->insert([
            'category' => 'Wild',
            
        ]);

    }
}
