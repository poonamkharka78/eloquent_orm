<?php

use Illuminate\Database\Seeder;

class WildAnimalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('wild_animals')->insert([
       		'cat_id' => 2,
            'animal_name' => 'Lion',
            'breed_id'=>4,
           
        ]);
       DB::table('wild_animals')->insert([
       		'cat_id' => 2,
            'animal_name' => 'Tiger',
            'breed_id'=>5,
           
        ]);
       DB::table('wild_animals')->insert([
       		'cat_id' => 2,
            'animal_name' => 'Elephant',
            'breed_id'=>6,
           
        ]);
    }
}
