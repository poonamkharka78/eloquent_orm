<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SubController\DataController@index');

Route::get('index-page','SubController\AminalDataController@index');
Route::get('domestic-data-page','SubController\AminalDataController@domesticData');
Route::get('wild-data-page','SubController\AminalDataController@wildData');
Route::get('dome-breed','SubController\AminalDataController@domeBreed');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace'=>'SubController'], function(){
	Route::get('index','RoleMasterController@index');
	Route::post('save-data','RoleMasterController@store');
	Route::get('signup-page','PersonController@index');
	Route::post('signup','PersonController@store');
	Route::get('listing','DataController@list');
});



Route::get('my-helper', function () {
    return view('myhelper');
});
