<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Eloquent</title>
        <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
       <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/">Eloquent</a>
            </div>
            <ul class="nav navbar-nav">
              <li class="active"><a href="/index">Add Role</a></li>
                <li><a href="/listing">View User List</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/signup-page"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
          </div>
        </nav>
        <div class="container">

           <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{url('signup')}}">
                        @csrf

                         <h3>Sign Up</h3>
                         <hr>

                         @if(Session::has('success'))
                              <div class="alert alert-success">{{Session::get('success')}}</div>
                        @endif
                        @if(Session::has('fail'))
                              <div class="alert alert-danger">{{Session::get('fail')}}</div>
                        @endif
                            <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"  autocomplete="name" autofocus>
                            @if($errors->first('name'))
                                <span style="color: red;">{{ $errors->first('name') }}</span>
                            @endif
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>

                                <div class="col-md-6">
                                    <select name="role_id" class="form-control" >
                                        <option>Select Role</option>
                                        @foreach($data as $data)
                                        <option value="{{ $data->id }}">{{ $data->role}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('role_id'))
                                        <span style="color: red;">{{ $errors->first('role_id') }}</span>
                                    @endif
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"   autocomplete="email">
                                @if($errors->first('email'))
                                        <span style="color: red;">{{ $errors->first('email') }}</span>
                                    @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password"  autocomplete="new-password">
                                @if($errors->first('password'))
                                        <span style="color: red;">{{ $errors->first('password') }}</span>
                                    @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

        </div>

    </body>
</html>
