<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Eloquent</title>
        <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
       <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/">Eloquent</a>
            </div>
            <ul class="nav navbar-nav">
              <li><a href="/index">Add Role</a></li>
                <li><a href="/listing">View User List</a></li>
            </ul>

              <div class="text-center">
                  <p style="color: white; font-weight: bolder;">{{ Helper::test('2019-09-24 18:10:50') }}</p>
              </div>

              <ul class="nav navbar-nav navbar-right">
              <li><a href="/signup-page"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
          </div>

        </nav>
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Role</th>
            </tr>
            </thead>
            <tbody>

            @foreach($viewData as $key=>$data)
               <tr>
                    <td>{{$data->persondata[0]->name}}</td>
                    <td>{{$data->rolesdata[0]->role}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
            </div>
        </body>
    </html>
