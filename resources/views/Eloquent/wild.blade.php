<!DOCTYPE html>
<html lang="en">
<head>
  <title>Eloquent</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  @foreach ($rawData as $data)
  <h2>{{ $data->category }}</h2>
   <small>(Aminals and their breeds)</small>
  <hr>
  <a href="/index-page" class="btn btn-success">Back</a>
  <hr>
  <table class="table">
    <thead>
      <tr class="danger">
        <th>{{$data->WildAnimalModel[1]->animal_name}}</th>
        <th>{{$data->WildAnimalModel[0]->animal_name}}</th>
        <th>{{$data->WildAnimalModel[2]->animal_name}}</th>
      </tr>
    </thead>
    <tbody>
      <tr class="info">
          @foreach($breedWild as $tag)
            @if($tag->id > 3)
               <td>{{ $tag->Breed}}</td>
            @endif
          @endforeach
      </tr>
    </tbody>
  </table>
  @endforeach
</div>
</body>
</html>
