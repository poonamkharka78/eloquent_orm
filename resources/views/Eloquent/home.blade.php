<head>
  <title>Eloquent</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style type="text/css">
  	.link{
  		text-decoration: none;
  		font-weight: bold;
  		font-size:18px; 
  		color: #000000;
  	}
  </style>
</head>
<body>

<div class="container">
  <h2>Animal</h2>
   <table class="table">
  	<h4>(Catgories)</h4>
  	<hr>
    <tbody>
      <ul>
        <li>
        	@foreach ($homeData as $data)
				<p>
				<a href="/domestic-data-page" class="link">{{ $data->category }}</a></p>
				@endforeach
		</li>
        <li>
        	@foreach ($home as $row)
				<p>
					<a href="/wild-data-page" class="link">{{ $row->category }}</a></p>
			@endforeach
		</li>
      </ul>      
    </tbody>
  </table>
</div>

</body>
</html>
