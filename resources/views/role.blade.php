<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Eloquent</title>
        <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
       <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/">Eloquent</a>
            </div>
            <ul class="nav navbar-nav">
              <li><a href="/index">Add Role</a></li>
                <li><a href="/listing">View User List</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/signup-page"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
          </div>
        </nav>

        <div class="container">
          <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <form method="post" action="{{url('save-data') }}">
                    @csrf
                    @if(Session::has('msg'))
                      <div class="alert alert-success">{{Session::get('msg')}}</div>
                    @endif
                    @if(Session::has('failmsg'))
                      <div class="alert alert-warning">{{Session::get('failmsg')}}</div>
                    @endif
                  <h4><b>*Role Master*</b></h4>
                  <hr>
                  <div class="form-group row">
                   <div class="col-xs-5">
                      <label for="role">Role</label>
                      <input type="text" name="role">
                       @if($errors->first('role'))
                           <p style="color:#ff0000">{{$errors->first('role')}}</p>
                           @endif
                    </div>
                    <button type="submit" class="btn-success">Add</button>
                   </div>
                 </form>
              </div>
              </div>
              </div>
            </div>
          </div>
     </body>
</html>
